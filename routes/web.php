<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'HomeController@index');
Route::get('/home', ['as' => 'home', 'uses' => 'HomeController@index']);

Route::get('/catalog', ['as' => 'catalog', 'uses' => 'HomeController@catalog']);
Route::get('/creative/new', ['as' => 'creative.new', 'uses' => 'HomeController@creativeNew']);
Route::post('/creative/new', ['as' => 'creative.new', 'uses' => 'HomeController@creativeCreate']);

Route::get('/ajax-get-models', ['as' => 'ajax-get-models', 'uses' => 'HomeController@ajaxGetModels']);
Route::get('/ajax-get-dict', ['as' => 'ajax-get-dict', 'uses' => 'HomeController@ajaxGetDict']);
