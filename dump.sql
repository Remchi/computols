-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 26, 2017 at 09:20 PM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `auto_qatestlab`
--

-- --------------------------------------------------------

--
-- Table structure for table `creatives`
--

CREATE TABLE `creatives` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` double(15,2) NOT NULL,
  `city` int(10) UNSIGNED NOT NULL,
  `owners_count` int(10) UNSIGNED NOT NULL,
  `make` int(10) UNSIGNED NOT NULL,
  `model` int(10) UNSIGNED NOT NULL,
  `engine` double(15,2) NOT NULL,
  `kms` double(15,2) NOT NULL,
  `owner_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `creatives`
--

INSERT INTO `creatives` (`id`, `title`, `description`, `price`, `city`, `owners_count`, `make`, `model`, `engine`, `kms`, `owner_id`, `created_at`, `updated_at`) VALUES
(4, 'Pontiac Other Pontiac Models', 'Perferendis commodi quidem quidem vitae consectetur iste dolore. Magnam et est et quidem. Officia sed illo sunt minus et. Maxime maxime voluptatum tenetur cumque adipisci eius nisi.\n\nItaque molestiae consequuntur molestiae aut. Ut adipisci ullam qui eligendi. Et numquam omnis ut aut quaerat. Quas ducimus nesciunt harum hic amet. Qui hic numquam quam saepe laborum voluptatum.\n\nEt libero repudiandae soluta reiciendis dignissimos dolorem nostrum. Ea itaque reiciendis enim culpa fuga eaque quae est. Ut provident voluptas sit eos fugit rem et.', 1.62, 38, 2, 1135, 1162, 3.50, 45235.90, 4, '2017-04-26 16:10:10', '2017-04-26 16:10:10'),
(5, 'Pontiac Other Pontiac Models', 'Aliquam non incidunt distinctio quibusdam nisi fuga quia. Nihil rerum id dolores pariatur et itaque ab. Illum est nihil ut nihil omnis. Doloremque sint non atque voluptas quod qui aut.\n\nDucimus magni temporibus enim eius sint. A porro deleniti nulla sed corporis sit officiis. Culpa sit earum non numquam dignissimos.\n\nVitae voluptatem quia et quas. Dolor rerum aliquid assumenda quo. Sint ullam dicta enim iste et. Alias qui sint et magni similique sint doloribus.', 429753.38, 38, 1, 1135, 1162, 2.40, 915.18, 4, '2017-04-26 16:10:10', '2017-04-26 16:10:10'),
(6, 'Mitsubishi Outlander', 'Excepturi omnis itaque adipisci ipsum est cum magnam. Quia debitis facilis recusandae. Aliquid velit animi odit eos rem est.\n\nSed quasi quam accusamus animi qui. Ipsa vitae neque accusantium magni id. Voluptatibus sint velit enim nisi voluptates quos perferendis.\n\nAt expedita sed velit repellendus harum enim. Voluptatem error alias non quibusdam. Sequi et tempora ex recusandae inventore ducimus fugiat. Nesciunt nostrum eveniet rem ut.', 43305598.45, 48, 5, 1034, 1049, 4.40, 511877650.38, 6, '2017-04-26 16:10:11', '2017-04-26 16:10:11'),
(7, 'Mitsubishi Outlander', 'Esse eligendi labore amet iusto dolore quis. Ducimus occaecati exercitationem sed molestias. Reprehenderit dolor voluptatem natus dicta rerum. Cupiditate vel laudantium facilis aut qui sunt.\n\nRecusandae veniam quo totam et. Doloremque ut odit eos. Sit sequi praesentium ea in.\n\nNihil ab voluptatibus aliquid qui. Quo et accusamus excepturi possimus qui dolores. Sit distinctio atque est maiores libero tempora.', 6.36, 48, 5, 1034, 1049, 3.10, 5666.93, 6, '2017-04-26 16:10:11', '2017-04-26 16:10:11'),
(8, 'AMC Encore', 'Sapiente aspernatur laborum mollitia. Quis nam illum dolores minus commodi quaerat dolore. Impedit non est officiis earum quibusdam vero at velit.\n\nQuas aut incidunt ipsum provident maxime cupiditate quam. Saepe sed excepturi dolorem ipsa asperiores. Nisi quibusdam aut ut aut saepe quam officia animi. Architecto distinctio doloribus aspernatur.\n\nEum quis provident sed qui. Molestiae qui vel culpa nam aperiam velit. Beatae quia et qui ut. Eos eos consequuntur sit ea consequuntur. Non eum sint est et dignissimos voluptatibus.', 7946.50, 36, 4, 33, 37, 3.80, 37.76, 7, '2017-04-26 16:10:11', '2017-04-26 16:10:11'),
(9, 'AMC Encore', 'Recusandae atque earum laudantium. Facilis nisi inventore in vel maxime in expedita quia. Totam magnam voluptatem non veritatis. Voluptas temporibus dolor dolorem officiis eligendi distinctio tenetur.\n\nEveniet dolor qui non sit ea sit quae. In eius at id minus deleniti occaecati eveniet. Rerum odio consequuntur dolores est repellat vel unde. Doloribus fuga dolor et deleniti.\n\nPorro sequi optio iusto. Similique sit officiis ratione debitis perspiciatis quia dolores eos. Eaque sit nam nihil error culpa minus.', 77507.69, 36, 2, 33, 37, 1.60, 311.88, 7, '2017-04-26 16:10:11', '2017-04-26 16:10:11'),
(10, 'AMC Encore', 'Numquam reprehenderit soluta nostrum nam aliquid omnis hic. Eos a placeat vel minima possimus. Incidunt quas dolorem aut nihil ut.\n\nCorrupti maiores perferendis est laboriosam. Expedita iusto nulla quaerat eius tenetur quas qui sed. Aperiam animi maiores consequatur quia id a commodi. Asperiores reprehenderit voluptas officia recusandae tempore.\n\nQui eos laudantium expedita voluptate minus impedit. Nihil libero dicta repellat quod quos dolore. Assumenda aliquam cum molestias aliquid.', 52500.97, 36, 9, 33, 37, 1.40, 7.61, 7, '2017-04-26 16:10:11', '2017-04-26 16:10:11'),
(11, 'HUMMER Other Hummer Models', 'Veniam repudiandae explicabo aut aut. Nostrum porro soluta rerum tempora. Et libero facilis voluptatem omnis sint aliquid.\n\nEsse ut ad qui. Omnis rem voluptas recusandae dolorum neque. Alias natus occaecati rerum mollitia repudiandae ipsa ex.\n\nAut dolores possimus repellat amet ipsam voluptates. Nisi distinctio repudiandae velit ex sit occaecati molestias. Quasi saepe sed est repellat.', 12238.24, 12, 2, 564, 569, 4.00, 129698584.17, 8, '2017-04-26 16:10:11', '2017-04-26 16:10:11'),
(12, 'HUMMER Other Hummer Models', 'Nam nostrum nihil similique. Eius aut dignissimos fugit exercitationem. Ratione aut adipisci eius ut cum autem recusandae. Qui tempore libero ea voluptas et.\n\nAb sed officiis cum id. Dolore aut repellat inventore. Recusandae aspernatur et dolore.\n\nEt inventore molestiae ullam facere quo quis culpa. Consequuntur quia quia ad quidem eaque laudantium minima.', 9451.38, 12, 2, 564, 569, 4.20, 32682.53, 8, '2017-04-26 16:10:11', '2017-04-26 16:10:11'),
(13, 'Jeep Wrangler', 'Quibusdam reiciendis dignissimos nobis ullam eos sed. Quia similique ad reprehenderit velit ut incidunt nesciunt. Veritatis minima et iusto. Debitis quia molestiae deleniti assumenda deserunt vitae.\n\nCorporis magnam molestiae dolor sit unde. Soluta explicabo nesciunt occaecati iusto. Exercitationem excepturi iusto illum quibusdam.\n\nDignissimos aut aut iste. Est commodi recusandae blanditiis.', 7.63, 9, 8, 664, 677, 1.90, 273806.98, 9, '2017-04-26 16:10:11', '2017-04-26 16:10:11'),
(14, 'Jeep Wrangler', 'Delectus quo enim molestiae et deserunt sunt. Non rem molestias sequi ipsa. Dolores provident quibusdam blanditiis ut eaque.\n\nNecessitatibus fugiat voluptas adipisci repellat error qui eum. Quae consectetur reprehenderit voluptas sint. Commodi commodi pariatur animi repudiandae. Et non dicta reprehenderit consequatur deserunt corporis in est.\n\nRerum soluta sit neque fuga reprehenderit tempora quo. Non ut voluptatem qui explicabo. Itaque consectetur et quisquam velit.', 53436317.24, 9, 7, 664, 677, 3.20, 621656.81, 9, '2017-04-26 16:10:11', '2017-04-26 16:10:11'),
(15, 'Jeep Wrangler', 'Aperiam minima perferendis architecto esse temporibus libero. Quo soluta non voluptas dolores similique praesentium explicabo. Ut porro libero officiis ipsa ut aliquam est. Reprehenderit commodi assumenda eius sequi.\n\nLaboriosam consectetur aliquid dicta voluptatem sint. Amet molestiae officiis eos blanditiis laborum pariatur. Qui quas sequi ipsam voluptatibus.\n\nExpedita illo deleniti at id ipsa. Doloremque praesentium est nesciunt blanditiis. Reprehenderit dolor harum itaque molestias autem qui. Dolor libero voluptatibus nisi voluptatem alias.', 754915014.94, 9, 7, 664, 677, 5.10, 3743.66, 9, '2017-04-26 16:10:11', '2017-04-26 16:10:11'),
(16, 'Chrysler E Class', 'Sapiente totam nulla officiis architecto. Dicta delectus sit voluptatem ut non et. Aliquam corporis voluptas praesentium voluptas voluptas. Dolores et consequuntur itaque occaecati illo quisquam qui.\n\nDolor at ea ipsum nemo molestias suscipit. Debitis similique amet et enim est asperiores dolores. Doloribus eius a iusto officiis assumenda et.\n\nLibero hic maiores beatae. Quaerat saepe repellendus est officiis.', 404771301.92, 7, 1, 302, 313, 2.40, 13291.85, 10, '2017-04-26 16:10:11', '2017-04-26 16:10:11'),
(17, 'Chrysler E Class', 'Non est saepe sed id. Quis ducimus sed commodi quo itaque. Iure autem illum nam neque iste ut.\n\nQuibusdam aut voluptas ad voluptatem fugiat blanditiis. Quas rem in esse minus et tempore esse magni. Consequatur corporis omnis accusamus deleniti.\n\nNam magni libero qui fugiat. Quidem ut qui est nobis expedita. Distinctio magnam dolor nemo enim. Molestiae quam ut aspernatur soluta consequatur aliquid occaecati.', 369.69, 7, 5, 302, 313, 3.30, 160306.91, 10, '2017-04-26 16:10:11', '2017-04-26 16:10:11'),
(18, 'Mercedes-Benz  - 500E', 'Porro praesentium quas qui ut aut illum asperiores. Voluptatem assumenda dolor dicta sed. Omnis non repellat possimus velit. Culpa dolor rerum nihil aut.\n\nNihil dolores et ut. Quibusdam eos vitae magni. Temporibus aspernatur facilis omnis et.\n\nMinima optio quam cumque commodi qui incidunt. Autem sit quas recusandae sunt voluptate. Mollitia tempora quaerat repudiandae beatae.', 538866542.06, 26, 8, 836, 888, 2.40, 25674.37, 11, '2017-04-26 16:10:11', '2017-04-26 16:10:11'),
(19, 'Mercedes-Benz  - 500E', 'Unde et libero quod. Eaque veritatis illum ad vitae commodi. Facere suscipit dolore quasi tempora ipsam ut aut nihil.\n\nEligendi animi quia consectetur esse laudantium sapiente excepturi. Et id rerum nihil aliquid voluptas deleniti illo placeat. Assumenda illum nihil est et labore commodi autem voluptatem.\n\nDistinctio beatae error harum voluptatem sed est. Dolorum ab eum eum reprehenderit nihil. Officia a est blanditiis laudantium. Quaerat rerum fugit aspernatur ipsam repellat error. Nihil ex voluptate accusamus et fuga debitis.', 1153459.78, 26, 7, 836, 888, 5.10, 7851.44, 11, '2017-04-26 16:10:11', '2017-04-26 16:10:11'),
(20, 'Saturn  - LW2', 'Minus eos fugiat ratione quis rerum sunt. Repellat alias et dolor molestias. Voluptatum iure saepe neque ratione.\n\nFuga quam est totam. Vitae iste et beatae. Id ex sit temporibus cum et.\n\nNesciunt repellendus veniam aliquid nihil optio enim sapiente. Voluptas maxime laudantium pariatur blanditiis quisquam praesentium nulla.', 34333916.50, 37, 3, 1207, 1218, 2.00, 199.97, 12, '2017-04-26 16:10:12', '2017-04-26 16:10:12'),
(21, 'Saturn  - LW2', 'Autem aut delectus fuga in cum voluptas. In veniam eos est. Quae nulla maxime dignissimos.\n\nVelit ut est nobis voluptates debitis at. Quibusdam aliquid eos vero. Doloribus distinctio inventore aut debitis quis voluptatem corrupti.\n\nEt iure ut nulla illo voluptatibus laboriosam et. Doloribus accusantium sed impedit exercitationem fuga ducimus. Nostrum nisi doloremque possimus autem consectetur et eius.', 726364.25, 37, 6, 1207, 1218, 4.10, 37732.98, 12, '2017-04-26 16:10:12', '2017-04-26 16:10:12'),
(22, 'Saturn  - LW2', 'Provident ullam deleniti repudiandae a laboriosam. Nisi illo minima placeat excepturi non. Omnis qui ipsum officiis quam neque nisi. At amet eaque dolores aut sit.\n\nAut placeat et consequatur nulla ut ratione. Consequatur inventore ipsum ratione nesciunt voluptas. Voluptatum aperiam animi magni voluptas. Nihil laboriosam facere quia necessitatibus excepturi iste ut.\n\nUnde ut voluptatum deleniti saepe. Consequatur ut ut quasi harum molestiae modi ut assumenda. Impedit velit cumque quod modi corrupti perferendis.', 4253.12, 37, 8, 1207, 1218, 3.50, 337613389.17, 12, '2017-04-26 16:10:12', '2017-04-26 16:10:12'),
(23, 'Chrysler Town & Country', 'Dolores corporis animi vitae magnam debitis corrupti. Debitis qui ipsam minus voluptate non. Architecto laudantium quis excepturi maxime deleniti provident sunt dolorum.\n\nNeque tenetur a omnis alias maiores. Aut ut repellendus maiores possimus placeat distinctio eaque. Delectus perferendis qui ea aut. Quam ut hic aliquid consectetur officia dolore.\n\nVoluptatem ex officia in fugiat eius non. Voluptas ut ut facilis qui amet. Temporibus similique dolorem facilis soluta aperiam unde voluptate.', 3683441.88, 3, 5, 302, 329, 2.20, 1959.03, 13, '2017-04-26 16:10:12', '2017-04-26 16:10:12'),
(24, 'Mercedes-Benz  - CL65 AMG', 'Voluptas et provident repellat. Amet voluptatum et aspernatur ex dolores facilis omnis. Numquam fugit corporis voluptatem eos et vel voluptatem.\n\nSunt nulla non ipsum. Inventore sunt quidem saepe reiciendis ratione quisquam perspiciatis.\n\nOdio doloremque delectus earum beatae quam et. Qui unde et tempora sapiente alias. Dolorem et et omnis ratione in quo. Ut sed unde alias molestiae distinctio molestiae aliquam quisquam.', 11128.73, 6, 7, 836, 869, 4.60, 15483182.78, 14, '2017-04-26 16:10:12', '2017-04-26 16:10:12'),
(25, 'Mercedes-Benz  - CL65 AMG', 'Soluta placeat deleniti consequatur commodi consequatur ea. Rerum quae eveniet quidem aperiam in iste est. Error non velit nesciunt corporis molestiae eos placeat fuga.\n\nPlaceat soluta dolores eveniet et dolores. Quaerat debitis maiores commodi at labore et. Officiis unde laudantium et recusandae.\n\nDolores quibusdam est maxime qui consequuntur qui. Et et vero quae molestiae cupiditate natus. Saepe dicta harum laudantium.', 79639.58, 6, 9, 836, 869, 4.00, 43488056.52, 14, '2017-04-26 16:10:12', '2017-04-26 16:10:12'),
(26, 'Mercedes-Benz  - CL65 AMG', 'Officiis qui non expedita nisi repellat non. Itaque libero tenetur sapiente ullam nostrum et explicabo. Ut qui dignissimos nostrum quos autem autem vel. Vitae qui id accusamus ratione ut et.\n\nQuis expedita aut non laboriosam. Distinctio consequatur et earum ratione cum natus vitae.\n\nSimilique porro odit non et sed rerum cumque cupiditate. Et qui quia recusandae corrupti rem aspernatur. Qui et possimus vel et ut voluptatem voluptas. Repudiandae natus voluptatem aut modi ipsum voluptatem tenetur pariatur.', 567.93, 6, 7, 836, 869, 3.10, 112281.60, 14, '2017-04-26 16:10:12', '2017-04-26 16:10:12'),
(27, 'Plymouth Acclaim', 'Cum cum temporibus ipsa inventore eligendi dolor dolor velit. Temporibus magnam ex recusandae est at. Similique voluptatem iste consequatur id quos in.\n\nBlanditiis nobis non aut commodi iure eum accusamus. Est similique repellat omnis laborum eius. Commodi et necessitatibus non cumque temporibus ducimus. Ipsum iure sunt vero consequatur vero.\n\nAperiam ipsum ut voluptates voluptatibus. Deserunt est architecto dicta soluta ratione eum. Perferendis non molestiae sint et aut iste.', 460942.81, 24, 7, 1114, 1115, 1.80, 2.89, 15, '2017-04-26 16:10:12', '2017-04-26 16:10:12'),
(28, 'Plymouth Acclaim', 'Sed dolorem reiciendis eaque et quidem. Dolores quod explicabo quasi et dolorem. Et maiores voluptatem placeat qui consequatur deleniti ipsa voluptatibus.\n\nVitae et deserunt labore velit. In qui alias sint saepe. Ad quis sapiente adipisci mollitia sunt incidunt. Ut sequi aut corporis optio ad et.\n\nVel nostrum dolore consequatur dolores. Quasi veritatis sed eligendi cumque accusantium. Et cum repellat ut nihil aspernatur omnis hic. Et nemo error eveniet nostrum nesciunt vel. Eligendi dolor repellat aut et.', 6090750.03, 24, 6, 1114, 1115, 2.00, 14.58, 15, '2017-04-26 16:10:12', '2017-04-26 16:10:12'),
(29, 'Plymouth Acclaim', 'Itaque voluptas reprehenderit odit vel. Velit possimus atque repellendus nisi qui. Qui qui ut sunt sapiente natus.\n\nEum voluptatem atque ut eius. Quia delectus inventore facilis facilis ut. Unde sint dolor eum quibusdam est aut. Dolores veritatis quis et qui dicta distinctio at.\n\nIpsa voluptas aut omnis ut. Qui consequatur aut in repellendus tenetur quo velit. Culpa dolor a aut sint impedit provident quibusdam.', 0.37, 24, 3, 1114, 1115, 3.60, 301.83, 15, '2017-04-26 16:10:12', '2017-04-26 16:10:12');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `creatives`
--
ALTER TABLE `creatives`
  ADD PRIMARY KEY (`id`),
  ADD KEY `creatives_owner_id_foreign` (`owner_id`),
  ADD KEY `creatives_city_foreign` (`city`),
  ADD KEY `creatives_make_foreign` (`make`),
  ADD KEY `creatives_model_foreign` (`model`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `creatives`
--
ALTER TABLE `creatives`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `creatives`
--
ALTER TABLE `creatives`
  ADD CONSTRAINT `creatives_city_foreign` FOREIGN KEY (`city`) REFERENCES `cities` (`id`),
  ADD CONSTRAINT `creatives_make_foreign` FOREIGN KEY (`make`) REFERENCES `models` (`id`),
  ADD CONSTRAINT `creatives_model_foreign` FOREIGN KEY (`model`) REFERENCES `models` (`id`),
  ADD CONSTRAINT `creatives_owner_id_foreign` FOREIGN KEY (`owner_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
