<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class CreativeCount
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {

        if (($user = Auth::user()) && $user->creative()->count() < 3) {
            return $next($request);
        }

        return redirect('/home')->withMessage(_('You already have max creatives.'));
    }
}
