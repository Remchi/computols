<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Builder as QueryBuilder;
use App\Creative;
use App\CitiesDictionary;
use App\CarModelsDictionary;

use Auth;
use Storage;
use Image;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth', [
            'only' => ['creativeNew', 'creativeCreate']
        ]);
        $this->middleware('creative_guard', [
            'only' => ['creativeNew', 'creativeCreate']
        ]);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $currentPage = 0;

        if ($request->has('page')) {
            $currentPage = $request->input('page');
        }

        $totalPages = intval(ceil(Creative::count() / 10.0));

        return view('home', compact(
            'currentPage', 'totalPages'
        ));
    }

    public function catalog(Request $request)
    {
        $query = Creative::take(10);

        $currentPage = 0;

        if ($request->has('page')) {
            $currentPage = $request->input('page');
        }

        if ($request->has('filter')) {
            $this->applyFilters($query, $request->input('filter'));
        }

        $totalPages = intval(ceil($query->count() / 10.0)) - 1;


        $creatives = $query->skip($currentPage * 10)
            ->orderBy('created_at', 'DESC')
            ->get();

        $url = $request->fullUrl();
        list($next_page, $prev_page) = $this->makePaginatorUrl($url, $currentPage, $totalPages);

        return json_encode([
            'creatives' => $creatives,
            'current_page' => $currentPage,
            'total_pages' => $totalPages,
            'prev_page_url' => $currentPage == 0
                ? ''
                : $prev_page,
            'next_page_url' => $currentPage == $totalPages
                ? ''
                : $next_page,
        ]);
    }

    public function creativeNew()
    {
        $cities = CitiesDictionary::all()->map(function($item) {
            if ($item->parent_id) {
                $item->title = '&nbsp;&nbsp;' . $item->title;
            }

            return $item;
        });

        $makes = CarModelsDictionary::whereNull('parent_id')->get();

        return view('creative.new', compact(
            'cities', 'makes'
        ));
    }

    public function creativeCreate(Request $request)
    {
        $data = $request->except('_token');
        $data['owner_id'] = Auth::user()->id;

        $creative = Creative::create($data);

        $dir_name = 'creative' . $creative->id;
        $creative_path = $dir_name . DIRECTORY_SEPARATOR;

        $storage = Storage::disk('uploads');

        for ($i = 0; $i < 5; $i++) {

            if ($request->hasFile('image' . $i)) {
                $image = $request->file('image' . $i);

                $filename = $i . '.' . $image->getClientOriginalExtension();

                if (!collect($storage->directories('.'))->has($dir_name)) {
                    $storage->makeDirectory($creative_path);
                }

                Image::make($image->getRealPath())->resize(200, 200)->encode('jpg')->save(public_path('uploads' . DIRECTORY_SEPARATOR . $creative_path . $filename));
            }
        }

        return redirect()->route('home');
    }

    public function ajaxGetModels(Request $request)
    {
        $models = [];
        if ($request->has('parent_id')) {
            $models = CarModelsDictionary::select(['id', 'title'])
                ->where(['parent_id' => $request->input('parent_id')])
                ->get();
        }

        return response()->json($models);
    }

    private function applyFilters(QueryBuilder $query, $data)
    {
        Creative::filter($query, $data);
    }

    private function makePaginatorUrl($base, $current, $total)
    {
        $url = preg_replace('/page=\d+/', '', $base);

        $amp = '?';
        if (strpos($url, '?')) {
            $amp = '&';
        }

        $next = $current + 1;
        $prev = $current + 1;


        $next = $next >= $total ? '' : $url . $amp . 'page=' . $next;
        $prev = $prev < 0       ? '' : $url . $amp . 'page=' . $prev;

        return [
            strtr($next, ['&&' => '&', '?&' => '?']),
            strtr($prev, ['&&' => '&', '?&' => '?'])
        ];

    }

    public function ajaxGetDict()
    {
        $cities = CitiesDictionary::pluck('title', 'id');
        $models = CarModelsDictionary::whereNull('parent_id')->pluck('title', 'id');

        return response()->json(compact('cities', 'models'));

    }
}
