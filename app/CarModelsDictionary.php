<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CarModelsDictionary extends Model
{
    protected $table = 'models';

    public $timestamps = false;

    public function make()
    {
        return $this->hasOne(self::class, 'id', 'parent_id');
    }
}
