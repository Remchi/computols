<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CitiesDictionary extends Model
{
    protected $table = 'cities';

    public $timestamps = false;
}
