<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Auth\Events\Registered;
use Validator;
use App\User;
use Illuminate\Validation\ValidationException;

class CreateUser extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user:create
                            {name : Set username}
                            {email : Set email}
                            {password : Set password}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $v = $this->validator([
                'name' => $this->argument('name'),
                'email' => $this->argument('email'),
                'password' => $this->argument('password')
            ]);
            $v->validate();
        }
        catch (ValidationException $e) {
            foreach ($v->messages()->all() as $message) {
                $this->error('Error: ' . $message);
            }

            exit();
        }

        event(new Registered($user = $this->create([
            'name' => $this->argument('name'),
            'email' => $this->argument('email'),
            'password' => $this->argument('password')
        ])));

        $this->info('User `' . $this->argument('name') . '` has been created.');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:60',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }
}
