<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder as QueryBuilder;

class Creative extends Model
{
    protected $fillable = [
        'title', 'description', 'price', 'owner_id',
        'kms', 'engine', 'owners_count',
        'city', 'make', 'model'
    ];

    protected static $filterable = [
        'price', 'city', 'make', 'model', 'kms', 'engine', 'owners_count'
    ];

    public static function filter(QueryBuilder $query, array $fields)
    {
        foreach ($fields as $field => $value) {
            if (is_null($value)) continue;

            if (self::fieldCanFiltered($field)) {

                if (strpos($value, ':') !== false) {
                    list($min, $max) = explode(':', $value);
                    if (($min || $min == 0) && $max) {
                        $query->whereBetween($field, [$min, $max]);
                    }
                    elseif ($min || $min == 0) {
                        $query->where($field, '>=', $min);
                    }
                    else {
                        $query->where($field, '<=', $max);
                    }
                }
                else {
                    $query->where([$field => $value]);
                }

            }
        }
    }

    private static function fieldCanFiltered($field)
    {
        return in_array($field, self::$filterable);
    }
}
