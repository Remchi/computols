Requirements:
=============

* npm
* composer
* php-gd

Install:
========

git clone https://bitbucket.org/Remchi/Computols.git
chmod 0777 bootstrap/cache
chmod 0777 storage -R
chmod 0777 public/uploads
composer install

`edit .env`

php artisan migrate --seed
...
npm run dev

...

Done.
