@extends('layouts.app')

@section('content')
  <style>
  span.load-button {
    width: 70px;
    height: 70px;
    border: 3px dashed #ccd0d2;
    margin-right: 20px;
    margin-bottom: 5px;
    display: inline-block;
    font-size: 58px;
    vertical-align: center;
    text-align: center;
    line-height: 60px;
    color: #ccd0d2;
  }
  span.load-button:last-of-type {
    margin-right: 0px;
  }
  .image-preview {
    max-height: 70px;
    max-width: 70px;
    display: inline-block;
    width: 70px;
    height: 70;
    text-align: center;
    position: relative;
    margin-top: -3px;
    margin-left: -3px;
  }

  span.load-button {
    display: none;
  }

  span.load-button:first-of-type {
    display: inline-block;
  }

  span.load-button.has-image + input + span.load-button {

    display: inline-block;
  }

  </style>
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="panel panel-default">
          <div class="panel-heading">Creative New</div>

          <div class="panel-body">

            <form action="" method="POST" enctype="multipart/form-data" id="creative">
              {{ csrf_field() }}

              <div class="form-group" class="form-horizontal">
                <div class="col-md-10 col-md-offset-2">
                  <label class="col-md-12 control-label" for="title">@lang('Title')</label>
                  <input class="form-control" type="text" id="title" name="title" required>
                </div>
              </div>

              <div class="form-group">
                <div class="col-md-10 col-md-offset-2">
                  <label class="col-md-12 control-label" for="price">@lang('Price')</label>
                  <input class="form-control" type="number" step="0.1" id="price" name="price" required>
                </div>
              </div>

              <div class="form-group">
                <div class="col-md-2">
                </div>

                <div class="col-md-10">
                  <label class="col-md-12 control-label">@lang('Images')</label>
                  @for ($i = 0; $i < 5; $i++)
                    <span class="load-button image{{ $i }}" data-index="{{ $i }}">
                      +
                    </span>
                    <input type="file" name="image{{ $i }}" id="image{{ $i }}" class="hidden" data-index="{{ $i }}" @if(!$i) required @endif>
                  @endfor

                  <span class="validation-error-image0"></span>
                </div>
              </div>

              <div class="form-group">
                <div class="col-md-5 col-md-offset-2">
                  <label class="col-md-12 control-label" for="city">@lang('City')</label>
                  <select id="city" name="city" class="form-control" required>
                    <option value="" disabled selected>---</option>
                    @foreach ($cities as $city)
                      <option value="{{ $city->id }}">{{ $city->title }}</option>
                    @endforeach
                  </select>
                </div>

                <div class="col-md-5">
                  <label class="col-md-12 control-label" for="owners">@lang('Owners count')</label>
                  <input class="form-control" type="number" step="0.1" id="owners" name="owners_count" required>
                </div>
              </div>

              <div class="form-group">
                <div class="col-md-5 col-md-offset-2">
                  <label class="col-md-12 control-label" for="make">@lang('Make')</label>
                  <select id="make" name="make" class="form-control" required>
                    <option value="" disabled selected>---</option>
                    @foreach ($makes as $make)
                      <option value="{{ $make->id }}">{{ $make->title }}</option>
                    @endforeach
                  </select>
                </div>
                <div class="col-md-5">
                  <label class="col-md-12 control-label" for="model">@lang('Model')</label>
                  <select id="model" name="model" class="form-control" required>
                    <option value="" disabled selected>---</option>
                  </select>
                </div>
              </div>

              <div class="form-group">
                <div class="col-md-5 col-md-offset-2">
                  <label class="col-md-12 control-label" for="engine">@lang('Engine capacity')</label>
                  <input class="form-control" type="number" step="0.1" id="engine" name="engine" required>
                </div>
                <div class="col-md-5">
                  <label class="col-md-12 control-label" for="kms">@lang('Mileage')</label>
                  <input class="form-control" type="number" step="0.1" id="kms" name="kms" required>
                </div>
              </div>


              <div class="form-group">
                <div class="col-md-10 col-md-offset-2">
                  <label class="col-md-12 control-label" for="description">@lang('Description')</label>
                  <textarea class="form-control" id="description" name="description" row="5" minlength="20" required>
                    @lang('Enter your description')
                  </textarea>
                </div>
              </div>

              <button type="submit">@lang('Submit')</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection

@push('scripts')
  <script>

    $('#make').change(function() {
      var $selected = $(this).find(':selected'),
        parent_id = $selected.val(),
        title = $selected.text();

      $.get('{{ route('ajax-get-models') }}', {'parent_id': parent_id})
        .done(function(response) {
          var Models = $('#model');

          Models.empty();
          Models.append($('<option/>').val("").attr('disablead', 'disabled').prop('selected', true).text('---'));

          $.each(response, function(_, item) {
            Models.append($('<option/>').val(item.id).text(title + ' ' + item.title));
          });
        });
    });

  $('.load-button').click(function()
  {
    var index = $(this).data('index')
    $('#image' + index).trigger('click');
  });

$('input[type=file]').change(function()
  {
  var input = this
  if (input.files && input.files[0]) {
    var reader = new FileReader();

    reader.onload = function (e) {
      var index = $(input).data('index')
      var $img = $('<img />')

      $img.addClass('image-preview').attr('src', e.target.result)
      $('.load-button.image' + index).html($img).addClass('has-image');
    };

    if (input.files[0].size <= 2000000) {
      reader.readAsDataURL(input.files[0]);
    }
    else {
      alert('Невозможно загрузить изображение. Размер изображения превышает 2Мб');
    }
  }
  });
$('form#creative button').on('click', function() {
      $('span.validation-error-image0').text('');
    if ($('form#creative input[type=file]#image0')[0].validity.valueMissing) {
      $('span.validation-error-image0').text(
        $('form#creative input[type=file]#image0')[0].validationMessage
      );
    }
  });
  </script>
@endpush
