@extends('layouts.app')

@push('scripts-head')
  <script>
    window.catalog = {
      url: "{{ route('catalog') }}",
      current: {{ $currentPage }},
      total: {{ $totalPages }}
    };
    window.dict = {
      url: "{{ route('ajax-get-dict') }}"
    };
  </script>
@endpush

@section('content')
  @if (session('message'))
    <div class="alert alert-info alert-dismissable">
      <a class="close" href="#" data-dismiss="alert" area-label="close">&times;</a>
      {{ session('message') }}
    </div>
  @endif

  <div id="app"></div>
@endsection
