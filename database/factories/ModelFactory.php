<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Creative::class, function(Faker\Generator $faker) {
    static $owner_id;

    $faker->addProvider(new CarFaker\Provider\Car($faker, new CarFaker\CarData));

    return [
        'title' => $faker->carModelVariant(),
        'description' => $faker->paragraphs(3, true),
        'price' => $faker->randomFloat(2, 1),
        'owners_count' => $faker->randomDigitNotNull(),
        'engine' => $faker->randomFloat(1, 1, 6),
        'kms' => $faker->randomFloat(2),
        'owner_id' => $owner_id ?: $owner_id = factory(App\User::class)->create()->id,
        'created_at' => date('Y-m-d H:i:s'),
        'updated_at' => date('Y-m-d H:i:s')
    ];
});
