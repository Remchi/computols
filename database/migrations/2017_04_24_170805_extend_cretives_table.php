<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ExtendCretivesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('creatives', function(Blueprint $table) {
            $table
                ->integer('city')
                ->unsigned()
                ->after('price');

            $table
                ->integer('owners_count')
                ->unsigned()
                ->after('city');

            $table
                ->integer('make')
                ->unsigned()
                ->after('owners_count');

            $table
                ->integer('model')
                ->unsigned()
                ->after('make');

            $table
                ->double('engine', 15, 2)
                ->after('model');

            $table
                ->double('kms', 15, 2)
                ->after('engine');

            $table->foreign('city')
                ->references('id')->on('cities');

            $table->foreign('make')
                ->references('id')->on('models');

            $table->foreign('model')
                ->references('id')->on('models');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('creatives', function(Blueprint $table) {
            $table->dropForeign(['city']);
            $table->dropForeign(['make']);
            $table->dropForeign(['model']);
        });

        Schema::table('creatives', function(Blueprint $table) {
            $table->dropColumn('city');
            $table->dropColumn('owners_count');
            $table->dropColumn('make');
            $table->dropColumn('model');
            $table->dropColumn('engine');
            $table->dropColumn('kms');
        });
       //
    }
}
