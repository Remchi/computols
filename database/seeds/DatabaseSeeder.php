<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(CarModelsSeeder::class);
        $this->call(CitiesSeeder::class);
        $this->call(TestUserSeeder::class);
        $this->call(TestCreativesSeeder::class);
    }
}
