<?php

use Illuminate\Database\Seeder;

class TestCreativesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $models = App\CarModelsDictionary::whereNotNull('parent_id')
            ->get()
            ->shuffle();
        $max_models = $models->count();

        $cities = App\CitiesDictionary::all()->shuffle();
        $max_cities = $cities->count();


        $creatives = collect([]);
        for ($i = 10; $i >= 0; $i--) {
            $owner_id = factory(App\User::class)->create()->id;

            $model = $models[mt_rand(0, $max_models - 1)];
            $title = $model->make->title . ' ' . $model->title;

            $city = $cities[mt_rand(0, $max_cities - 1)];

            $creatives = $creatives->merge( factory(App\Creative::class, (mt_rand() % 3 + 1))->create([
                'title' => $title,
                'make' => $model->make->id,
                'model' => $model->id,
                'city' => $city->id,
                'owner_id' => $owner_id
            ])->each(function($i) {
                $dir = public_path('uploads' . DIRECTORY_SEPARATOR . 'creative' . $i->id);
                mkdir($dir, '0777');
                copy(public_path('test.jpg'), $dir . DIRECTORY_SEPARATOR . '0.jpg');
            }));
        }
    }
}
