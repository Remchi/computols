<?php

use Illuminate\Database\Seeder;

class CitiesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->cities as $state) {
            $parent_id = \App\CitiesDictionary::create(['title' => $state['title']])->id;
            if (isset($state[0])) {
                foreach ($state[0] as $city) {
                    \App\CitiesDictionary::create(['title' => $city['title'], 'parent_id' => $parent_id]);
                }
            }
        }
    }

    private $cities = [
        [ 'title' => 'Киев' ],
        [
            [
                [ 'title' => 'Белая Церковь' ],
                [ 'title' => 'Бровары' ],
                [ 'title' => 'Украинка' ],
                [ 'title' => 'Вишнёвое' ],
            ],
            'title' => 'Киевская обл.',
        ],
        [
            [
                [ 'title' => 'Севастополь' ],
                [ 'title' => 'Симферополь' ],
            ],
            'title' => 'АР Крым',
        ],
        [ 'title' => 'Винница' ],
        [
            [
                [ 'title' => 'Каменское (Днепродзержинск)' ],
                [ 'title' => 'Кривой Рог' ],
                [ 'title' => 'Никополь' ],
                [ 'title' => 'Павлоград' ],
                [ 'title' => 'Жёлтые Воды' ],
            ],
            'title' => 'Днепр (Днепропетровск)',
        ],
        [
            [
                [ 'title' => 'Бахмут (Артемовск)' ],
                [ 'title' => 'Горловка' ],
                [ 'title' => 'Краматорск' ],
                [ 'title' => 'Макеевка' ],
                [ 'title' => 'Мариуполь' ],
                [ 'title' => 'Покровск' ],
            ],
            'title' => 'Донецк',
        ],
        [
            [
                [ 'title' => 'Коростень' ],
            ],
            'title' => 'Житомир',
        ],
        [
            [
                [ 'title' => 'Бердянск' ],
                [ 'title' => 'Энергодар' ],
                [ 'title' => 'Мелитополь' ],
                [ 'title' => 'Васильевка' ],
            ],
            'title' => 'Запорожье',
        ],
        [ 'title' => 'Ивано-Франковск' ],
        [
            [
                [ 'title' => 'Александрия' ],
            ],
            'title' => 'Кропивницкий (Кировоград)',
        ],
        [
            [
                [ 'title' => 'Лисичанск' ],
                [ 'title' => 'Северодонецк' ],
                [ 'title' => 'Старобельск' ],
            ],
            'title' => 'Луганск',
        ],
        [
            [
                [ 'title' => 'Ковель' ],
            ],
            'title' => 'Луцк',
        ],
        [
            [
                [ 'title' => 'Борислав' ],
                [ 'title' => 'Броды' ],
                [ 'title' => 'Червоноград' ],
                [ 'title' => 'Дрогобич' ],
                [ 'title' => 'Стрый' ],
                [ 'title' => 'Яворов' ],
            ],
            'title' => 'Львов',
        ],
        [ 'title' => 'Николаев' ],
        [
            [
                [ 'title' => 'Измаил' ],
            ],
            'title' => 'Одесса',
        ],
        [
            [
                [ 'title' => 'Кременчуг' ],
                [ 'title' => 'Лубны' ],
            ],
            'title' => 'Полтава',
        ],
        [
            [
                [ 'title' => 'Дубно' ],
            ],
            'title' => 'Ровно',
        ],
        [ 'title' => 'Сумы' ],
        [
            [
                [ 'title' => 'Бучач' ],
            ],
            'title' => 'Тернополь',
        ],
        [
            [
                [ 'title' => 'Хуст' ],
                [ 'title' => 'Мукачево' ],
                [ 'title' => 'Тячев' ],
                [ 'title' => 'Виноградов' ],
            ],
            'title' => 'Ужгород',
        ],
        [
            [
                [ 'title' => 'Купянск' ],
            ],
            'title' => 'Харьков',
        ],
        [
            [
                [ 'title' => 'Каховка' ],
            ],
            'title' => 'Херсон',
        ],
        [
            [
                [ 'title' => 'Каменец-Подольский' ],
                [ 'title' => 'Красилов' ],
            ],
            'title' => 'Хмельницкий',
        ],
        [
            [
                [ 'title' => 'Смела' ],
                [ 'title' => 'Умань' ],
            ],
            'title' => 'Черкассы',
        ],
        [ 'title' => 'Чернигов' ],
        [    'title' => 'Черновцы',]
    ];
}
